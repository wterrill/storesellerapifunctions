/* eslint-disable promise/catch-or-return */
/* eslint-disable promise/always-return */
const functions = require("firebase-functions")
const axios = require("axios")
const forIn = require("lodash/forIn")
const cors = require("cors")({
  origin: true
})
const cookieParser = require("cookie-parser")()
//------------------- BIG QUERY -------------------------
const {
  BigQuery
} = require("@google-cloud/bigquery")
const bigquery = new BigQuery()
//-------------------------------------------------------
//-------------------  FIREBASE  ------------------------
const admin = require("firebase-admin")
admin.initializeApp()
//-------------------------------------------------------
//-------------------  EXPRESS   ------------------------
const express = require("express")
const app = express()
//-------------------------------------------------------
//-------------------  IMAGEUPLOAD  ---------------------
const Busboy = require("busboy")
const fs = require("fs")
const stream = require("stream")
const {
  Storage
} = require("@google-cloud/storage")
const os = require("os")
const path = require("path")
//-------------------------------------------------------

const validateFirebaseIdToken = async (req, res, next) => {
  console.log("Check if request is authorized with Firebase ID token")

  if (
    (!req.headers.authorization ||
      !req.headers.authorization.startsWith("Bearer ")) &&
    !(req.cookies && req.cookies.__session)
  ) {
    console.error(
      "No Firebase ID token was passed as a Bearer token in the Authorization header.",
      "Make sure you authorize your request by providing the following HTTP header:",
      "Authorization: Bearer <Firebase ID Token>",
      'or by passing a "__session" cookie.'
    )
    res.status(403).send("Unauthorized")
    return
  }

  let idToken
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer ")
  ) {
    console.log('Found "Authorization" header')
    // Read the ID Token from the Authorization header.
    idToken = req.headers.authorization.split("Bearer ")[1]
    console.log("token", idToken)
  } else if (req.cookies) {
    console.log('Found "__session" cookie')
    // Read the ID Token from cookie.
    idToken = req.cookies.__session
  } else {
    // No cookie
    res.status(403).send("Unauthorized")
    return
  }

  try {
    const decodedIdToken = await admin.auth().verifyIdToken(idToken)
    console.log("ID Token correctly decoded", decodedIdToken)
    req.user = decodedIdToken
    next()
    return
  } catch (error) {
    console.error("Error while verifying Firebase ID token:", error)
    res.status(403).send("Unauthorized")
    return
  }
}
app.use(cors)
app.use(cookieParser)
//app.use(validateFirebaseIdToken)
app.get("/getProductsWithCategories", validateFirebaseIdToken, (req, res) => {
  let param = req.query.param
  let string

  if (param && param !== "" && param.length > 2) {
    string = `SELECT brand,channel1,discountPriceChannel1,id,image,lastModifiedAt,name,published,regularPriceChannel1,sku FROM \`commercetoolstottus.cl.productReport\` where LOWER(brand) LIKE lower('%${param}%') OR LOWER(sku) LIKE lower('%${param}%') or LOWER(name) LIKE lower('%${param}%') limit 50`
  } else {
    string = `SELECT brand,channel1,discountPriceChannel1,id,image,lastModifiedAt,name,published,regularPriceChannel1,sku FROM \`commercetoolstottus.cl.productReport\` limit 50`
  }
  console.log("String", string)
  const options = {
    query: string,
    location: "southamerica-east1"
  }
  async function query() {
    const [job] = await bigquery.createQueryJob(options).catch(err => {
      console.error("Error:", err)
    })
    console.log(`Job ${job.id} started.`)
    const [rows] = await job.getQueryResults().catch(err => {
      console.error("err2", err)
    })
    return rows
  }

  async function main() {
    const data = await query()
    return res.status(200).json({
      data
    })
  }
  main()
})
app.get("/getCategory", validateFirebaseIdToken, (req, res) => {
  if (req.method !== "GET") {
    return res.status(500).json({
      message: "Not allowed"
    })
  }
  const {
    query = {}
  } = req
  const {
    param = ""
  } = query
  if (typeof param === "undefined") {
    return res.status(400).json({
      status: "error",
      code: 400,
      message: "param is not defined"
    })
  }
  let string = `select distinct B.name as sonName, B.id as sonId, A.name as parentName , A.id as parentId from \`commercetoolstottus.cl.categoryReport\` A, \`commercetoolstottus.cl.categoryReport\` B where B.parentId = A.id and B.parentId is not null and lower(b.name) like lower('%${param}%')`
  const options = {
    query: string,
    location: "southamerica-east1"
  }
  async function bigqueryRequest() {
    const [job] = await bigquery.createQueryJob(options).catch(err => {
      console.error("createQueryJob Error:", err)
    })
    console.log(`Job ${job.id} started.`)
    const [rows] = await job.getQueryResults().catch(err => {
      console.error("getQueryResults Error:", err)
    })
    return rows
  }

  async function main() {
    const data = await bigqueryRequest()
    console.log("DATA", data)
    return res.status(200).json({
      data
    })
  }
  main()
})
app.get("/chatbot", validateFirebaseIdToken, (req, res) => {
  if (req.method !== "GET") {
    return res.status(500).json({
      message: "Not allowed"
    })
  }
  const {
    query = {}
  } = req
  const {
    rut = ""
  } = query
  let string = `select rut, orden,fecha_compra, monto, estado, fechaEntrega, pedido, PARSE_DATE('%d/%m/%Y',  fechaEntrega) as date from \`commercetoolstottus.cl.chatbot\` where rut = '${rut}' group by rut, orden,fecha_compra, monto, estado, fechaEntrega, pedido order by date desc`
  const options = {
    query: string,
    location: "southamerica-east1"
  }
  async function bigqueryRequest() {
    const [job] = await bigquery.createQueryJob(options).catch(err => {
      console.error("createQueryJob Error:", err)
    })
    console.log(`Job ${job.id} started.`)
    const [rows] = await job.getQueryResults().catch(err => {
      console.error("getQueryResults Error:", err)
    })
    return rows
  }

  async function main() {
    const data = await bigqueryRequest()
    console.log("DATA", data)
    return res.status(200).json({
      data
    })
  }
  main()
})
app.get("/getParentLine", validateFirebaseIdToken, (req, res) => {
  if (req.method !== "GET") {
    return res.status(500).json({
      message: "Not allowed"
    })
  }
  const {
    query = {}
  } = req
  const {
    param = ""
  } = query
  async function queryName(param) {
    let string = `select * from \`commercetoolstottus.cl.parentCategories\` where lower(title) like lower('%${param}%')`
    console.log("String", string)
    const options = {
      query: string,
      location: "southamerica-east1"
    }
    const [job] = await bigquery.createQueryJob(options).catch(err => {
      console.error("createQueryJob Error:", err)
    })
    console.log(`Job ${job.id} started.`)
    const [rows] = await job.getQueryResults().catch(err => {
      console.error("getQueryResults Error:", err)
    })
    return rows
  }

  async function main() {
    const results = await queryName(param).catch(() => {
      return []
    })
    return res.status(200).json({
      results
    })
  }
  main()
})
app.get("/getMargenes", validateFirebaseIdToken, (req, res) => {
  if (req.method !== "GET") {
    return res.status(500).json({
      message: "Not allowed"
    })
  }
  //const { query = {} } = req
  //const { param = "" } = query
  async function queryName() {
    let string = `select * from \`commercetoolstottus.cl.margenes\``
    console.log("String", string)
    const options = {
      query: string,
      location: "southamerica-east1"
    }
    const [job] = await bigquery.createQueryJob(options).catch(err => {
      console.error("createQueryJob Error:", err)
    })
    console.log(`Job ${job.id} started.`)
    const [rows] = await job.getQueryResults().catch(err => {
      console.error("getQueryResults Error:", err)
    })
    return rows
  }

  async function main() {
    const results = await queryName().catch(() => {
      return []
    })
    return res.status(200).json({
      results
    })
  }
  main()
})
app.get("/getProductChannels", validateFirebaseIdToken, (req, res) => {
  const {
    query = {}
  } = req
  const {
    sku = ""
  } = query
  if (typeof sku === "undefined") {
    return res.status(400).json({
      status: "error",
      code: 400,
      message: "missing sku value"
    })
  }
  async function querySku(sku) {
    const string = `select distinct A.sku as sku,  A.name  as name ,A.key as description , 
                    B.channel as channel, B.centAmount as price from \`commercetoolstottus.cl.productReport\` A, \`commercetoolstottus.cl.productReportChannels\` B 
                    where A.sku = B.productSku and sku like '%${sku}%' order by A.sku`

    const options = {
      query: string,
      location: "southamerica-east1"
    }
    const [job] = await bigquery.createQueryJob(options).catch(err => {
      console.error("createQueryJob Error:", err)
    })
    console.log(`Job ${job.id} started.`)
    const [rows] = await job.getQueryResults().catch(err => {
      console.error("getQueryResults Error:", err)
    })
    return rows
  }

  async function parseResults(rows) {
    console.log(rows);
    const {
      sku = "", name = "", description = ""
    } = rows[0]
    let newel = {}
    newel["sku"] = sku
    newel["name"] = name
    newel["description"] = description
    let allchannels = ["Canal CMR", "Canal precios Tottus Ya", "Canal de Precios Actual", "Canal TottusYa CMR"]
    let mychannels = rows.map(e => {
      return e.channel
    })
    allchannels.forEach(e => {
      if (!mychannels.includes(e)) {
        newel[e] = null
      }
    })
    rows.forEach((e) => {
      const {
        channel = "", price = 1
      } = e;
      newel[channel] = price
    })
    console.log(newel);
    return newel
  }
  async function main() {
    const call = await querySku(sku).catch((err) => {
      console.log(err);
      return []
    })
    const results = await parseResults(call).catch((err) => {
      console.log(err);
      return []
    })
    if (typeof results !== "undefined") {
      return res.status(200).json({
        status: "ok",
        results
      })
    } else {
      return res.status(400).json({
        status: "error",
        code: 400,
        message: "error parsing or fetching information from commerceTools"
      })
    }
  }
  main()
})
app.get("/testPickingCenter", validateFirebaseIdToken, (req, res) => {

  const {
    query = {}
  } = req
  const {
    sku = ""
  } = query
  async function queryPicking(sku) {
    let string = ""
    if (typeof sku === "undefined" || sku === "" || sku === "undefined") {
      string = `SELECT sku,
                MAX(IF(channel = '1285552b-3f02-4fd1-9ad8-df70358e04a8', availableQuantity, NULL)) AS channel1,
                MAX(IF(channel = '6870ed05-a561-4cda-abf9-a047dbd9a958', availableQuantity, NULL)) AS channel2,
                MAX(IF(channel = 'cb09c1c1-2063-401a-9e64-08fd96da3672', availableQuantity, NULL)) AS channel3,
                MAX(IF(channel = 'd1f982c0-d792-4173-893f-0e090a72accd', availableQuantity, NULL)) AS channel4
                FROM commercetoolstottus.cl.stockReport group by sku limit 200`
      console.log(string);
    } else {
      string = `SELECT sku,
                MAX(IF(channel = '1285552b-3f02-4fd1-9ad8-df70358e04a8', availableQuantity, NULL)) AS channel1,
                MAX(IF(channel = '6870ed05-a561-4cda-abf9-a047dbd9a958', availableQuantity, NULL)) AS channel2,
                MAX(IF(channel = 'cb09c1c1-2063-401a-9e64-08fd96da3672', availableQuantity, NULL)) AS channel3,
                MAX(IF(channel = 'd1f982c0-d792-4173-893f-0e090a72accd', availableQuantity, NULL)) AS channel4
                FROM commercetoolstottus.cl.stockReport where sku like '%${sku}%' group by sku limit 200`
      console.log(string);
    }
    const options = {
      query: string,
      location: "southamerica-east1"
    }
    const [job] = await bigquery.createQueryJob(options).catch(err => {
      console.error("createQueryJob Error:", err)
    })
    console.log(`Job ${job.id} started.`)
    const [rows] = await job.getQueryResults().catch(err => {
      console.error("getQueryResults Error:", err)
    })
    return rows
  }
  async function main() {
    const result = await queryPicking(sku).catch(err => {
      console.log(err);
      return {
        err: "error fetching the information"
      }
    })
    return res.status(200).json({
      results: result
    })
  }
  main()
})
app.get("/testStoredSellSku", async (req,res) =>{
  let token = req.headers.authorization
  //let sku = req.query.sku
  let fechaInicio = new Date(req.query.fechaInicio).toISOString()
  let fechaTermino = new Date(req.query.fechaTermino).toISOString()
  let finalarr= []
  //validation elements
  const validate  = [token,fechaInicio,fechaTermino].every((e)=>{return typeof e !== "undefined"})
  if(!validate){return res.status(400).json({ok:false,message:"missing parameters or headers"})}
  //----------------------
  token = token.split("Bearer ")[1]
  const firstcall = await axios({
    method:"GET",
    url:"https://staging.tottus.cl/s/orders/v1/",
    params:{
      startCreationDate:fechaInicio,
      endCreatinDate:fechaTermino,
      perPage:500,
      page:1
    },
    headers:{
      Authorization:`Bearer ${token}`
    }
  }).catch((err)=>{return res.status(500).json({ok:false,message:err.message})})
  //tomar todas las listas entre estas fechas
  //llamadas consecutivas para completar la venta aquí
  //=============================================================================================

  //=============================================================================================
  const results = firstcall.data.results
  const parsed = results.map((e)=>{
    const {id="",channel="", lineItems=[]} = e
    return lineItems.map((el)=>{
      const { variant = {},productId="", quantity = 0} = el
      const { prices =[], sku="" } = variant
      const {value = {}} = prices[0]
      const {fractionDigits = 0,currencyCode="",preciseAmount, centAmount = 0} = value
      let newPreciseAmount = typeof preciseAmount === "undefined" ? centAmount : preciseAmount
      newPreciseAmount /= (10**fractionDigits)
      const obj = {
        sku,
        quantity,
        price:newPreciseAmount,
        total:(newPreciseAmount*quantity),
      }
      finalarr.push(obj)
      return obj
    })
  })
  //
  const finalresult = {
    fechaInicio,
    fechaTermino,
    ventasPreliminar: finalarr.map(e => e.total).reduce((acu,el) => acu + el),
    products:finalarr
  }
  return res.status(200).json({
    ok:true,
    results:finalresult
  })
})
//===========================================================================
//                          CMS / TABLET AND IMAGES
//===========================================================================
app.get("/testcms", async (req, res) => {
  const result = await axios({
    //url: "https://commercetoolstottus.firebaseio.com/catalogProducts.json",
    url: "https://commercetoolstottus.firebaseio.com/StoreProducts.json",
    method: "GET"
  }).catch((err) => {
    return res.status(400).json({
      status: 400,
      msg: "error fetching data from catalogProducts",
      error: err
    })
  })
  let arr = []
  forIn(result.data, (e)=>{
    arr.push(e)
  })
  return res.status(200).json({
    status: 200,
    results: arr
  })
})


app.get("/healthTabletApp", async (req, res) => {
  const versions = await axios({
    url: "https://commercetoolstottus.firebaseio.com/tabletVersions.json",
    method: "get"
  }).catch(() => {
    return res.status(500).json({
      status: 500,
      url: "https://http.cat/500",
      message: "couldnt comunicate with the server"
    })
  })
  const iterable = versions.data
  let listOfVersion = {}
  forIn(iterable, (element) => {
    const { ultimateVersion = 1, minimalVersion = 1 } = element

    if (ultimateVersion !== 1) {
      listOfVersion["ultimateVersion"] = ultimateVersion
    }
    if (minimalVersion !== 1) {
      listOfVersion["minimalVersion"] = minimalVersion
    }
  })
  return res.status(200).json({
    status: 200,
    results: listOfVersion
  })
})

app.post("/postStoreSellerSKU", async(req, res) => {
  // This is used from the "store Seller page to upload Familia and SKUs to the server"
  let message = req.body ;
  res.status(200).send(message);
  console.log("received store seller data for SKUs")
  const snapshot = await admin.database().ref('/SKUs').set(message);
  // This works with: curl -H "Accept: application/json" -H "Content-tyT -d '{"message":"Sloop"}' https://us-central1-commercetoolstottus.cloudfunctions.net/app/beers
});


app.post("/postAppProducts", async(req, res) => {
  // This is used from the "store Seller page to upload Familia and SKUs to the server"
  let message = req.body ;
  res.status(200).send(message);
  console.log("received app data data for from node server")
  const snapshot = await admin.database().ref('/StoreProducts').set(message);
  // This works with: curl -H "Accept: application/json" -H "Content-tyT -d '{"message":"Sloop"}' https://us-central1-commercetoolstottus.cloudfunctions.net/app/beers
});

app.post("/postToCatalog", async(req, res) => {
  // This is used from the "store Seller page to upload Familia and SKUs to the server"
  let message = req.body ;
  res.status(200).json(message);
  console.log("received catalog (long term) product data")
  console.log(message)
  const snapshot = await admin.database().ref('/catalogProducts').update(message);
  // This works with: curl -H "Accept: application/json" -H "Content-tyT -d '{"message":"Sloop"}' https://us-central1-commercetoolstottus.cloudfunctions.net/app/beers
});

app.get("/getStoreSeller", async (req, res) => {
  const result = await axios({
    url: "https://commercetoolstottus.firebaseio.com/SKUs.json",
    method: "GET"
  }).catch((err) => {
    return res.status(400).json({
      status: 400,
      msg: "error fetching data for Store Seller SKUs",
      error: err
    })
  })
  let arr = []
  forIn(result.data, (e)=>{
    arr.push(e)
  })
  return res.status(200).json({
    status: 200,
    results: arr
  })
})

exports.uploadProductImage = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    if (req.method !== "POST") {
      return res.status(500).json({
        message: "Not allowed"
      })
    }
    //https://b2b-tottus.firebaseio.com/users/J581Acizo7eWBT0NxtIEOYGrqvo2/comunaDestinatario
    const busboy = new Busboy({ headers: req.headers })
    let uploadData = null

    let fields = {
      base64Image: "",
      sku: ""
    }

    busboy.on("field", (fieldName, fieldValue, valTruncated, keyTruncated) => {
      if (fieldName === "sku") {
        fields.sku = fieldValue
      }
    })

    busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
      const filepath = path.join(os.tmpdir(), filename)
      uploadData = { file: filepath, type: mimetype, filename: filename }
      file.pipe(fs.createWriteStream(filepath))
    })

    busboy.on("finish", () => {
      const storage = new Storage()
      storage
        .bucket("gs://commercetoolstottus.appspot.com")
        .upload(uploadData.file, {
          uploadType: "media",
          metadata: {
            metadata: {
              contentType: uploadData.type
            }
          }
        })
        .catch(err => {
          return res.status(500).json({
            error: err
          })
        })
      const bucket = storage.bucket("gs://commercetoolstottus.appspot.com")
      const file = bucket.file(uploadData.filename)
      const config = {
        action: "read",
        expires: "03-17-2025"
      }
      const getImageUrl = (file, config) => {
        file.getSignedUrl(config, (err, url) => {
          if (err) {
            return err
          }
          console.log("PASA POR ACA: 3 ")
          // admin
          //   .database()
          //   .ref(`/users/${userId}/oc/${ocId}/url`)
          //   .set(url)
          return res.status(200).json({
            message: "Image successfully uploaded",
            url: url
          })
        })
      }
      const signedUrl = getImageUrl(file, config)
    })
    busboy.end(req.rawBody)
  })
})
//===========================================================================

//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
app.get("/getLogisticsTrackBigQuery", async (req, res) => {
  	console.log("started getLogisticsTrackBigQuery")
    let query = 'SELECT * FROM `tottus-chile.espacio.Logistics2`';
    bigquery.query(query).then((err, rows) => {
        if (!err) {
            console.log("success!")
            console.log(rows)
            console.log(rows[0])
            return res.status(200).json({
                status: 200,
                results: rows[0]
              })
        } else {
        
        console.log("err")
        console.log(err)
          return res.status(400).json({
      status: 401,
      msg: "error fetching data for logistics track OCs",
      error: err
    })
        }
    }).catch(err => {
        console.log("ERROR ERROR")
        console.log(err)
      return res.status(400).json({
      status: 400,
      msg: "error fetching data for logistics track OCs",
      error: err
    })
    });
  })

app.post("/postLogisticsTrack", async(req, res) => {
  // This is used from the "store Seller page to upload Familia and SKUs to the server"
  let message = req.body ;
  res.status(200).send(message);
  console.log("received logistics track data")
  console.log(message)
  const snapshot = await admin.database().ref('/logisticsTrack').update(message);
  // This works with: curl -H "Accept: application/json" -H "Content-tyT -d '{"message":"Sloop"}' https://us-central1-commercetoolstottus.cloudfunctions.net/app/beers
});

app.get("/getLogisticsTrack", async (req, res) => {
  const result = await axios({
    url: "https://commercetoolstottus.firebaseio.com/logisticsTrack.json",
    method: "GET"
  }).catch((err) => {
    return res.status(400).json({
      status: 400,
      msg: "error fetching data for logistics track OCs",
      error: err
    })
  })
  let arr = []
  forIn(result.data, (e)=>{
    arr.push(e)
  })
  return res.status(200).json({
    status: 200,
    results: arr
  })
})
//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
exports.app = functions.https.onRequest(app)